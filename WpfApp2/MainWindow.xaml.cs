﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Framework.Controller;
using Framework.Command;
using System.Linq;

namespace WpfApp2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : WindowBase
    {
        public ICommand Save { get; set; }
        public ICommand Reset { get; set; }
        public ICommand TestMsgr { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            Save = new RelayCommand(() => {
                Console.WriteLine("SAVE DATA");
            });

            Reset = new RelayCommand(() => {
                Console.WriteLine("RESET DATA");
            });

            TestMsgr = new RelayCommand(() => {
                App.Msgr.NotifyColleagues(App.MSG_TEST_MESSENGER);
            });

            App.Msgr.Register(App.MSG_TEST_MESSENGER, () => { Console.WriteLine("Call messenger"); });

            /* USERS */
            App.Msgr.Register(App.MSG_ADD_USER, AddTabAddUSer);
            App.Msgr.Register<Model.User>(App.MSG_EDIT_USER, AddTabEditUser);

            var tab = new TabItem()
            {
                Header = "Users",
                Content = new View.UserList()
            };

            var tab2 = new TabItem()
            {
                Header = "Rendez-vous",
                Content = new View.RdvList()
            };

            tabControl.Items.Add(tab);
            tabControl.Items.Add(tab2);

            DataContext = this;
        }

        private void AddTabAddUSer() 
        {
            Model.User user = new Model.User();
            ManageUserTab(user, true);
        }

        private void AddTabEditUser(Model.User user) 
        {
            ManageUserTab(user, false);
        }

        private void ManageUserTab(Model.User user, bool isNew) 
        {
            if (user != null) 
            {
                var tab = (from TabItem t in tabControl.Items
                          where (string)t.Header ==
                          (isNew ? Properties.Resources.AddUser : Properties.Resources.EditUser + " " + user.ID.ToString())
                          select t).FirstOrDefault();

                if (tab == null)
                {
                    CreateTabForUser(user, isNew);
                }
                else 
                {
                    tab.Focus();
                }
            }
        }

        private void CreateTabForUser(Model.User user, bool isNew) 
        {
            object Header = (isNew ? Properties.Resources.AddUser : Properties.Resources.EditUser + " " + user.ID.ToString());
            object Content = new View.EditUser(user, isNew);
            CreateNewTab(Header, Content);
        }

        private void CreateNewTab(object Header, object Content) 
        {
            var tab = new TabItem()
            {
                Header = Header,
                Content = Content
            };
            tab.MouseDown += (o, e) =>
            {
                if (e.ChangedButton == MouseButton.Middle && e.ButtonState == MouseButtonState.Pressed) 
                {
                    tabControl.Items.Remove(o);
                }
            };
            tab.KeyDown += (o, e) =>
            {
                if (e.Key == Key.W && Keyboard.IsKeyDown(Key.LeftCtrl)) 
                {
                    tabControl.Items.Remove(o);
                }
            };
            tabControl.Items.Add(tab);
            tab.Focus();
        }
    }
}
