﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Framework.Controller;
using Framework.Command;

namespace WpfApp2.View
{
    /// <summary>
    /// Interaction logic for UserList.xaml
    /// </summary>
    public partial class UserList : UserControlBase
    {
        public ObservableCollection<Model.User> Users { get; set; }

        public Model.User SelectedUser { get; set; }

        public ICommand AddUser { get; set; }
        public ICommand EditUser { get; set; }

        public UserList()
        {
            InitializeComponent();

            Users = new ObservableCollection<Model.User>(App.Mdl.Users);

            SelectedUser = null;

            AddUser = new RelayCommand(() => 
            {
                App.Msgr.NotifyColleagues(App.MSG_ADD_USER);
            });

            EditUser = new RelayCommand<Model.User>((u) =>
            {
                App.Msgr.NotifyColleagues(App.MSG_EDIT_USER, SelectedUser);
            });

            DataContext = this;
        }
    }
}
