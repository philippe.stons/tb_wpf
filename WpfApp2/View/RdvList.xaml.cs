﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Framework.Controller;
using System.Linq;

namespace WpfApp2.View
{
    /// <summary>
    /// Interaction logic for RdvList.xaml
    /// </summary>
    public partial class RdvList : UserControlBase
    {
        public ObservableCollection<Model.RDV> Rdvs { get; set; }

        public RdvList()
        {
            InitializeComponent();

            var tempRdv = from r in App.Mdl.RDVs
                          orderby r.Date
                          select r;

            Rdvs = new ObservableCollection<Model.RDV>(tempRdv);

            DataContext = this;
        }
    }
}
