﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfApp2.Model
{
    public class User
    {
        public int ID { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Email { get; set; }
        public int AnneeDeNaissance { get; set; }
    }
}
