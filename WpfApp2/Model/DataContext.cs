﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfApp2.Model
{
    public class DataContext
    {
        public List<User> Users;
        public List<RDV> RDVs;

        public DataContext()
        {
            this.Users = new List<User>();

            this.Users.AddRange(new User[] {
                    new User() { ID = 1,  Nom = "Person", Prenom = "Michael", Email = "michael.person@cognitic.be", AnneeDeNaissance = 1982 },
                    new User() { ID = 2,  Nom = "Morre", Prenom = "Thierry", Email = "thierry.morre@cognitic.be", AnneeDeNaissance = 1974 },
                    new User() { ID = 3,  Nom = "Dupuis", Prenom = "Thierry", Email = "thierry.dupuis@cognitic.be", AnneeDeNaissance = 1988 },
                    new User() { ID = 4,  Nom = "Faulkner", Prenom = "Stephane", Email = "stephane.faulkner@cognitic.be", AnneeDeNaissance = 1969 },
                    new User() { ID = 5,  Nom = "Selleck", Prenom = "Tom", Email = "tom.selleck@email.com", AnneeDeNaissance = 1945 },
                    new User() { ID = 6,  Nom = "Anderson", Prenom = "Richard Dean", Email = "richard.dean.anderson@email.com", AnneeDeNaissance = 1950 },
                    new User() { ID = 7,  Nom = "Bullock", Prenom = "Sandra", Email = "sandra.bullock@hotmail.com", AnneeDeNaissance = 1964 },
                    new User() { ID = 8,  Nom = "Peppard", Prenom = "George", Email = "george.peppard@hotmail.com", AnneeDeNaissance = 1928 },
                    new User() { ID = 9,  Nom = "Estevez", Prenom = "Emilio", Email = "emilio.estevez@hotmail.com", AnneeDeNaissance = 1962 },
                    new User() { ID = 10,  Nom = "Moore", Prenom = "Demi", Email = "demi.moore@gmail.com", AnneeDeNaissance = 1962 },
                    new User() { ID = 11,  Nom = "Willis", Prenom = "Bruce", Email = "bruce.willis@gmail.com", AnneeDeNaissance = 1955 }
                });

            this.RDVs = new List<RDV>();

            this.RDVs.AddRange(
                    new RDV[]
                    {
                        new RDV() { ID = 4, Email = "stephane.faulkner@cognitic.be", Date = new DateTime(2012, 5, 12) },
                        new RDV() { ID = 8, Email = "george.peppard@hotmail.com", Date = new DateTime(2011, 8, 14) },
                        new RDV() { ID = 11, Email = "bruce.willis@gmail.com", Date = new DateTime(2012, 6, 19) },
                        new RDV() { ID = 11, Email = "bruce.willis@gmail.com", Date = new DateTime(2012, 6, 20) },
                        new RDV() { ID = 1, Email = "michael.person@cognitic.be", Date = new DateTime(2012, 4, 19) },
                    }
                );
        }
    }
}
