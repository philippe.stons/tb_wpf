﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfApp2.Model
{
    public class RDV
    {
        public int ID { get; set; }
        public string Email { get; set; }
        public DateTime Date { get; set; }
    }
}
