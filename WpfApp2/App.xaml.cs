﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Framework.Messenger;

namespace WpfApp2
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static Model.DataContext Mdl { get; set; /* Read only for real database */ } = new Model.DataContext();

        public static Messenger Msgr { get; } = new Messenger();

        public const string MSG_TEST_MESSENGER = "MSG_TEST_MESSENGER";

        /* USERS */
        #region USERS
        public const string MSG_EDIT_USER = "MSG_EDIT_USER";
        public const string MSG_ADD_USER = "MSG_ADD_USER";
        #endregion

        public App() 
        {
            Msgr.Register(MSG_TEST_MESSENGER, TestMessenger);
        }

        private void TestMessenger() 
        {
            Console.WriteLine("MESSENGER");
        }
    }
}
